import io
import picamera
import logging
import socketserver
from threading import Condition
import time
import grovepi
from http import server
#HTML page put into a string variable that is going to be used as the template for the webserver 
PAGE="""\
<html>
<head>
<title>Raspberry Pi Camera Streaming To Phone</title>
</head>
<link
      rel="stylesheet"
      href="/home/pi/Desktop/stylesheet.css"/>

<body style="  margin: 0;
  padding: 0;
  font-family: 'Poppins', sans-serif;
  box-sizing: border-box;"> 
    <div class="hero" style="width: 100%;
  min-height: 100vh;
  background: #dae4ff;
  padding-left: 6%;
  padding-right: 6%;">
      <nav style="
        width: 100%;
  padding: 45px 0px;
  display: flex;
  align-items: center;
      ">
        <div class="logo" style="flex: 1;">
          <i class="fas fa-home fa-3x" style="  width: 170px;
  cursor: pointer;"></i>
        </div>
        <button type="button" style="
          padding: 15px 40px;
  border: 0;
  outline: none;
  border-radius: 25px;
  background: #333;
  color: #fff;
  font-size: 14px;
  cursor: pointer;
  visibility:hidden;
  box-shadow: 0 10px 10px rgba(0, 0, 0, 0.2);
        ">List Property</button>
        <i class="fas fa-bars menu-icon fa-3x" style="
          width: 50px;
  margin-left: 30px;
  // border-radius: 50%;
  // box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  cursor: pointer;
        "></i>
      </nav>

      <div class="row" style="  display: flex;
  width: 70%;
  background: #fff;
  border-radius: 15px;
  overflow: hidden;">
        <div class="col-1" style="  flex-basis: 50%;
  overflow: hidden;">
          <h1 style="  font-size: 60px;
  line-height: 80px;
  letter-spacing: 2px;
  color: #333;
  margin-left: 35px;">
            Doorbell <br />
            Smart Camera
          </h1>
          <p style="  color: #777;
  line-height: 22px;
  margin: 15px 0px 30px;
  margin-left: 35px
  ">
          Smart home solutions that you can trust, powered by PI!
          </p>
          <button type="button" id='script' name="scriptbutton" value=" Run Script " onclick="goPython()" style="  padding: 15px 40px;
  border: 0;
  outline: none;
  border-radius: 25px;
  background: #333;
  color: #fff;
  font-size: 14px;
  margin-left: 35px;
  cursor: pointer;
  box-shadow: 0 10px 10px rgba(0, 0, 0, 0.2);">Click to Unlock Door</button>

          <ul style="margin-top:40px">
            <li class="btn active" style="  list-style: none;
  width: 15px;
  height: 15px;
  display: inline-block;
  background: #bfbfbf;
  border-radius: 50%;
  margin-right: 15px;
  cursor: pointer;
    background: #333;"></li>
            <li class="btn" style="
              list-style: none;
  width: 15px;
  height: 15px;
  display: inline-block;
  background: #bfbfbf;
  border-radius: 50%;
  margin-right: 15px;
  cursor: pointer;
            "></li>
          </ul>
        </div>

        <div class="col-2" style="  flex-basis: 50%;
  overflow: hidden;">
            <img src="stream.mjpg" width="640" height="480" />
        </div>
      </div>


    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <script>
        function goPython(){
            $.ajax({
              url: "/home/pi/Desktop/unlockPy.py",
             context: document.body
            }).done(function() {
             alert('finished python script');;
            });
        }
    </script>













</body>
</html>
"""
#Create a steaming object  that takes each frame from a video object and writes it to an streamable video for a website
class StreamingOutput(object):
    def __init__(self):
        self.frame = None
        self.buffer = io.BytesIO()
        self.condition = Condition()

    def write(self, buf):
        if buf.startswith(b'\xff\xd8'):
            # New frame, copy the existing buffer's content and notify all
            # clients it's available
            self.buffer.truncate()
            with self.condition:
                self.frame = self.buffer.getvalue()
                self.condition.notify_all()
            self.buffer.seek(0)
        return self.buffer.write(buf)
#Stream handeler which is able to create a website webserver and input as a variable the stream object
class StreamingHandler(server.BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/':
            self.send_response(301)
            self.send_header('Location', '/index.html')
            self.end_headers()
        elif self.path == '/index.html':
            content = PAGE.encode('utf-8')
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.send_header('Content-Length', len(content))
            self.end_headers()
            self.wfile.write(content)
        elif self.path == '/stream.mjpg':
            self.send_response(200)
            self.send_header('Age', 0)
            self.send_header('Cache-Control', 'no-cache, private')
            self.send_header('Pragma', 'no-cache')
            self.send_header('Content-Type', 'multipart/x-mixed-replace; boundary=FRAME')
            self.end_headers()
            try:
                while True:
                    with output.condition:
                        output.condition.wait()
                        frame = output.frame
                    self.wfile.write(b'--FRAME\r\n')
                    self.send_header('Content-Type', 'image/jpeg')
                    self.send_header('Content-Length', len(frame))
                    self.end_headers()
                    self.wfile.write(frame)
                    self.wfile.write(b'\r\n')
            except Exception as e:
                logging.warning(
                    'Removed streaming client %s: %s',
                    self.client_address, str(e))
        else:
            #Run Update unlock code when input from button click that isnt other server pushthrough
            print("hello")
            buzzer = 8
            grovepi.pinMode(buzzer,"OUTPUT")
            try:
                #Run UnlockPi and also unlock sound
                #UnlockPi() 
                grovepi.digitalWrite(buzzer,1)
                print ('start')
                time.sleep(1)
                grovepi.digitalWrite(buzzer,0)
                print ('stop')
                time.sleep(1)
            except:
                self.send_error(404)
                self.end_headers()
#The Server declarations and makking sure the server is using the correct address and server threads.
class StreamingServer(socketserver.ThreadingMixIn, server.HTTPServer):
    allow_reuse_address = True
    daemon_threads = True
#Use the piCamera files to get Camera, Create a streamingOutput Object and pass in the PiCamera Video
with picamera.PiCamera(resolution='640x480', framerate=24) as camera:
    output = StreamingOutput()
    camera.start_recording(output, format='mjpeg')
    try:
        address = ('', 8000)
        server = StreamingServer(address, StreamingHandler)
        server.serve_forever()
    finally:
        camera.stop_recording()