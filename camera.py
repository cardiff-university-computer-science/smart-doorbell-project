from picamera import PiCamera
from time import sleep
camera = PiCamera()
camera.rotation = 0
camera.brightness =70
camera.start_preview()
camera.image_effect = 'sketch'
camera.exposure_mode = 'antishake'
sleep(10)
camera.start_recording('/home/pi/Desktop/video.h264')
sleep(10)
camera.stop_recording()
camera.stop_preview()