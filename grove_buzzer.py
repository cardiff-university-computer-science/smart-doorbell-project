#!/usr/bin/env python
#
# GrovePi Example for using the Grove Buzzer (http://www.seeedstudio.com/wiki/Grove_-_Buzzer)
#
# The GrovePi connects the Raspberry Pi and Grove sensors.  You can learn more about GrovePi here:  http://www.dexterindustries.com/GrovePi
#
# Have a question about this example?  Ask on the forums here:  http://forum.dexterindustries.com/c/grovepi
#
'''
## License

The MIT License (MIT)

GrovePi for the Raspberry Pi: an open source platform for connecting Grove Sensors to the Raspberry Pi.
Copyright (C) 2017  Dexter Industries

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''
import lcd
import time
import grovepi
#Write to file the correct text for LCD
f = open("screentext.txt", "w")
f.write("Click button to ring")
f.close()

# Connect the Grove Buzzer to digital port D8
# SIG,NC,VCC,GND
buzzer = 8
button = 4
grovepi.pinMode(buzzer,"OUTPUT")
grovepi.pinMode(button, "INPUT")
#Run the LCD file to initicate PiGrove LCD to default output
exec(open("/home/pi/Desktop/lcd.py").read())
while True:
    try:
        #Wait for user to click the Pi Button
        if(grovepi.digitalRead(button) == 0):
        # Buzz for 1 second
        
            f = open("screentext.txt", "w")
            f.write("Ringing")
            f.close()
            grovepi.digitalWrite(buzzer,1)
            print ('start')
            
            time.sleep(1)
            grovepi.digitalWrite(buzzer,0)
            print ('stop')
            time.sleep(1)
            x = 'hello'
            #Set LCD for Ringing and Unlocking 
            exec(open("lcd.py").read())
        
        # Stop buzzing for 1 second and repeat
            #
            time.sleep(5)
            f = open("screentext.txt", "w")
            f.write("Click button to ring")
            f.close()
            #Return to default state
            exec(open("/home/pi/Desktop/lcd.py").read())
            

    except KeyboardInterrupt:
        grovepi.digitalWrite(buzzer,0)
        break
    except IOError:
        print ("Error")
        
    

def getX():
    global x
    b = x
    print(b)
    return b